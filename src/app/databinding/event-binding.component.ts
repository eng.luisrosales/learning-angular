import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'fa-event-binding',
  template: `
    <button (click)="onClicked()">Click Me!</button>
  `,
  styles: []
})
export class EventBindingComponent {

  //our own event
  @Output() clicked = new EventEmitter<string>();
  onClicked() {
    //alert('It Worked!');
    this.clicked.emit('It works');
  }
}
