import { Component,
         OnInit,
         OnChanges,
         DoCheck,
         AfterContentInit,
         AfterContentChecked,
         AfterViewInit,
         AfterViewChecked,
         OnDestroy,
         Input,
         ViewChild,
         ContentChild
       } from '@angular/core';

@Component({
  selector: 'fa-lifecycle',
  template: `
    <ng-content></ng-content>
    <hr>
    <p #boundParagraph>{{bindable}}</p>
    <p>{{boundParagraph.textContent}}</p>
  `,
  styles: []
})
export class LifecycleComponent implements OnChanges, OnInit, DoCheck, 
                                           AfterContentInit, AfterContentChecked, 
                                           AfterViewInit, AfterViewChecked, OnDestroy {

  @Input() bindable = 1000;

  @ViewChild('boundParagraph') //el argumento debe coincidir con el nombre de la variable local en el template 
  boundParagraph: HTMLElement;

  @ContentChild('boundContent') //el argumento debe coincidir con el nombre de la variable local en el template 
  boundContent: HTMLElement;

  constructor() { }

  ngOnChanges() {
    this.log('ngOnChanges');
  }

  ngOnInit() {

    this.log('ngOnInit');
    
  }

  ngDoCheck() {

    this.log('ngDoCheck');
  
  }

  ngAfterContentInit() {

    this.log('ngAfterContentInit');
    console.log(this.boundContent);
  }

  ngAfterContentChecked() {

    this.log('ngAfterContentChecked');
  
  }

  ngAfterViewInit() {

    this.log('ngAfterViewInit');
    console.log(this.boundParagraph);
  
  }

  ngAfterViewChecked() {

    this.log('ngAfterViewChecked');
  
  }

  ngOnDestroy() {

    this.log('ngOnDestroy');
  
  }

  private log(hook: string){
    console.log(hook);  
  }


}
